<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qtoken\model\token;

use Qerapp\qtoken\model\token\entity\TokenEntity,
    Qerapp\qtoken\model\token\mapper\TokenMapper,
    Qerapp\qtoken\model\token\interfaces\TokenMapperInterface,
    Qerapp\qtoken\model\token\repository\TokenRepository;
use Ada\adapters\XmlAdapter,
    Ada\adapters\PDOAdapter;

//RELATED-MAPPERS

/*
  |*****************************************************************************
  | [{server_name}]
  |*****************************************************************************
  |
  | Service for Entity TokenService
  | @author qDevTools,
  | @date 2020-02-20 06:25:19,
  |*****************************************************************************
 */

class TokenService {

    private
            $_Token;
    public
            $TokenRepository;

    public function __construct(TokenMapperInterface $Mapper = null) {

        try {
            $TokenMapper = new TokenMapper(new PDOAdapter(\Ada\SqlPDO::singleton(), 'qtokens'));
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Mapper.TokenService', $ex);
        }

        $MapperRepository = (is_null($Mapper)) ? $TokenMapper : $Mapper;
        $this->TokenRepository = new TokenRepository($MapperRepository);
    }

    /**
     * Create token activation for user
     * @param int $id_user 
     * @param int $type (1:user activation,2: user recovery,3:etc) 
     */
    public function createUserToken(int $id_user, int $type = 1) {

        $UserToken = new TokenEntity();
        $UserToken->id_user = $id_user;
        $UserToken->generateToken();
        $UserToken->setAddress();
        $UserToken->setAgent();
        $UserToken->setDate();
        $UserToken->token_type = filter_var($type, FILTER_SANITIZE_NUMBER_INT);

        // store
        $this->TokenRepository->store($UserToken);
        $this->_Token = $UserToken;
    }

    /**
     *  Verify token
     * @param string $token
     */
    public function verify(string $token) {


        // comment this line at 10 april 2020
//        $Session = new \Qerana\core\QSession();
//        $Session->cleanSession();

        $seperate_token = wordwrap($token, 30, ',', true);

        // convert to array
        $token_parts = explode(',', $seperate_token);

        if (count($token_parts) != 2) {
            \QException\Exceptions::showError('Qtoken.Invalid', 'Token no valido!!');
        }

        $token_string = $token_parts[0];

        $Token = $this->TokenRepository->findByToken($token_string);

        if ($Token instanceof interfaces\TokenInterface) {

            // get coeficient
            $coeficient = 7 * $Token->token_type;
            $id_user = $token_parts[1] / $coeficient;

            if ($id_user == $Token->id_user) {
                $this->_Token = $Token;

                // if is a reactivation token check ip and user agent
                if ($this->_Token->token_type == 2) {

                    $remote_ip = filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_VALIDATE_IP);
                    $remote_agent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_SANITIZE_SPECIAL_CHARS);

                    // check ip
                    if ($this->_Token->remote_address_token !== $remote_ip) {
                        \QException\Exceptions::showError('Qtoken.InvalidIP', 'La IP no es la misma que origino el token ' . $this->_Token->remote_address_token);
                    }

                
                }
            } else {
                \QException\Exceptions::showError('Qtoken.Invalid', 'Token no valido para este usuario!!');
            }
        } else {
            \QException\Exceptions::showError('Qtoken.Invalid', 'Token no existe!!');
        }
    }

    public function getToken() {
        return $this->_Token;
    }

    /**
     * -------------------------------------------------------------------------
     * Save 
     * -------------------------------------------------------------------------
     */
    public function save(array $data = []) {

        $data_to_save = (empty($data)) ? \helpers\Request::getFormData() : $data;

        $Token = new TokenEntity($data_to_save);
        $this->TokenRepository->store($Token);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete 
     * -------------------------------------------------------------------------
     * @entity tokenentity
     * @return type
     */
    public function delete($Token) {

        return $this->TokenRepository->remove($Token);
    }

}
