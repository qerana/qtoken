<?php

/*
 * Copyright (C) 2019/20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qtoken\model\token\entity;

use \Ada\EntityManager,
    Qerapp\qtoken\model\token\interfaces\TokenInterface;

//RELATED-NS


/*
  |*****************************************************************************
  | ENTITY CLASS for Token
  |*****************************************************************************
  |
  | Entity Token
  | @author qDevTools,
  | @date 2020-02-20 06:25:19,
  |*****************************************************************************
 */

class TokenEntity extends EntityManager implements TokenInterface, \JsonSerializable
{

    protected

    //ATRIBUTES        

    /** @var int(11), $id_token  */
            $_id_token,
            /** @var varchar(45), $id_user  */
            $_id_user,
            /** @var varchar(30), $token  */
            $_token,
            /** @var tinyint(1), $token_type  */
            $_token_type,
            /** @var int(10), $lifetime  */
            $_lifetime,
            /** @var text(), $user_agent  */
            $_user_agent,
            /** @var varchar(100), $remote_address_token  */
            $_remote_address_token,
            /** @var timestamp(), $sys_timestamp_token  */
            $_sys_timestamp_token,
            /** @var datetime(), $sys_token_creationdate  */
            $_sys_token_creationdate,
            /** @var tinyint(1), $token_used  */
            $_token_used,
            /** @var text , token info json format*/
            $_token_info;
    public
    /**
     *  @Entity User related for token
     */
            $User;

    //RELATED

    public function __construct(array $data = [])
    {
        $this->populate($data);

        //RELATED-LOAD
    }

    /**
     *  Get related user
     */
    public function getUser()
    {
        $this->User = \Ada\EntityRelation::hasOne($this, 'user', 'id_user');
    }

    /**
     *  token as a random  string(30) join with id_user multiplied for 7
     */
    public function generateToken(): void
    {
        $this->_token = \helpers\Security::random(30);
    }
    
    public function setAddress(){
        $this->_remote_address_token = filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_VALIDATE_IP);
    }
    
    public function setAgent(){
        $this->_user_agent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_SANITIZE_SPECIAL_CHARS);
    }
    
    public function setDate(){
        $this->_sys_token_creationdate = date('Y-m-d H:i:s');
    }

//SETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_token
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_token($id_token = 0): void
    {
        $this->_id_token = filter_var($id_token, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_user
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_id_user($id_user = ""): void
    {
        $this->_id_user = filter_var($id_user, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for token
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_token($token = ""): void
    {
        $this->_token = (empty($token)) ? $this->generateToken() : filter_var($token, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for token_type
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_token_type($token_type = ""): void
    {
        $this->_token_type = filter_var($token_type, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for lifetime
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_lifetime($lifetime = 0): void
    {
        $this->_lifetime = filter_var($lifetime, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for user_agent
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_user_agent($user_agent = ""): void
    {
        $this->_user_agent = filter_var($user_agent, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for remote_address_token
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_remote_address_token($remote_address_token = ""): void
    {
        $this->_remote_address_token = filter_var($remote_address_token, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for sys_timestamp_token
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_sys_timestamp_token($sys_timestamp_token = ""): void
    {
        $this->_sys_timestamp_token = filter_var($sys_timestamp_token, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for sys_token_creationdate
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_sys_token_creationdate($sys_token_creationdate = ""): void
    {
        try {
            $this->_sys_token_creationdate = \helpers\Date::toDate($sys_token_creationdate);
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException("DateError", $ex);
        }
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for token_used
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_token_used($token_used = ""): void
    {
        $this->_token_used = filter_var($token_used, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for token_info
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_token_info($token_info = ""): void
    {
        $this->_token_info = filter_var($token_info, FILTER_SANITIZE_STRING);
    }

//GETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_token
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_token()
    {
        return $this->_id_token;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_user
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_id_user()
    {
        return filter_var($this->_id_user, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for token
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_token()
    {
        return filter_var($this->_token, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for token_type
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_token_type()
    {
        return $this->_token_type;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for lifetime
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_lifetime()
    {
        return $this->_lifetime;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for user_agent
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_user_agent()
    {
        return filter_var($this->_user_agent, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for remote_address_token
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_remote_address_token()
    {
        return filter_var($this->_remote_address_token, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for sys_timestamp_token
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_sys_timestamp_token()
    {
        return $this->_sys_timestamp_token;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for sys_token_creationdate
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_sys_token_creationdate()
    {
        return \helpers\Date::toString($this->_sys_token_creationdate, "d-m-Y H:i:s");
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for token_used
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_token_used()
    {
        return $this->_token_used;
    }
    /**
     * ------------------------------------------------------------------------- 
     * Getter for token_info
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_token_info()
    {
        return $this->_token_info;
    }

}
