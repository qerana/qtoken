<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qtoken\model\token\repository;

use Qerapp\qtoken\model\token\interfaces\TokenMapperInterface,
    Qerapp\qtoken\model\token\interfaces\TokenRepositoryInterface,
    Qerapp\qtoken\model\token\interfaces\TokenInterface;

/*
  |*****************************************************************************
  | TokenRepositoryRepository
  |*****************************************************************************
  |
  | Repository TokenRepository
  | @author qDevTools,
  | @date 2020-02-20 06:25:19,
  |*****************************************************************************
 */

class TokenRepository implements TokenRepositoryInterface
{

    private
            $_TokenMapper;

    public function __construct(TokenMapperInterface $Mapper)
    {

        $this->_TokenMapper = $Mapper;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all TokenRepository
     * -------------------------------------------------------------------------
     * @return TokenRepositoryEntity collection
     */
    public function findById(int $id)
    {
        return $this->_TokenMapper->findOne(['id_token' => $id]);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all TokenRepository
     * -------------------------------------------------------------------------
     * @return TokenRepositoryEntity collection
     */
    public function findAll(array $conditions = [], array $options = [])
    {
        return $this->_TokenMapper->findAll($conditions, $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_token
     * ------------------------------------------------------------------------- 
     * @param id_token 
     */
    public function findById_token(int $id_token, array $options = [])
    {
        return $this->_TokenMapper->findAll(['id_token' => $id_token], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_user
     * ------------------------------------------------------------------------- 
     * @param id_user 
     */
    public function findById_user(string $id_user, array $options = [])
    {
        return $this->_TokenMapper->findAll(['id_user' => $id_user], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  token
     * ------------------------------------------------------------------------- 
     * @param token 
     */
    public function findByToken(string $token, array $options = [])
    {
        return $this->_TokenMapper->findOne(['token' => $token], $options);
    }

    /**
     *   find by token and id_user
     * @param string $token
     * @param int $id_user
     * @return type
     */
    public function findByTokenAndUser(String $token, int $id_user)
    {
        return $this->_TokenMapper->findOne(['token' => $token, 'id_user' => $id_user]);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  token_type
     * ------------------------------------------------------------------------- 
     * @param token_type 
     */
    public function findByToken_type(string $token_type, array $options = [])
    {
        return $this->_TokenMapper->findAll(['token_type' => $token_type], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  lifetime
     * ------------------------------------------------------------------------- 
     * @param lifetime 
     */
    public function findByLifetime(int $lifetime, array $options = [])
    {
        return $this->_TokenMapper->findAll(['lifetime' => $lifetime], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  user_agent
     * ------------------------------------------------------------------------- 
     * @param user_agent 
     */
    public function findByUser_agent(string $user_agent, array $options = [])
    {
        return $this->_TokenMapper->findAll(['user_agent' => $user_agent], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  remote_address_token
     * ------------------------------------------------------------------------- 
     * @param remote_address_token 
     */
    public function findByRemote_address_token(string $remote_address_token, array $options = [])
    {
        return $this->_TokenMapper->findAll(['remote_address_token' => $remote_address_token], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  sys_timestamp_token
     * ------------------------------------------------------------------------- 
     * @param sys_timestamp_token 
     */
    public function findBySys_timestamp_token(string $sys_timestamp_token, array $options = [])
    {
        return $this->_TokenMapper->findAll(['sys_timestamp_token' => $sys_timestamp_token], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  sys_token_creationdate
     * ------------------------------------------------------------------------- 
     * @param sys_token_creationdate 
     */
    public function findBySys_token_creationdate(string $sys_token_creationdate, array $options = [])
    {
        return $this->_TokenMapper->findAll(['sys_token_creationdate' => $sys_token_creationdate], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  token_used
     * ------------------------------------------------------------------------- 
     * @param token_used 
     */
    public function findByToken_used(string $token_used, array $options = [])
    {
        return $this->_TokenMapper->findAll(['token_used' => $token_used], $options);
    }

    /**
     * -------------------------------------------------------------------------
     * Save TokenRepository
     * -------------------------------------------------------------------------
     * @object $TokenRepositoryEntity
     * @return type
     */
    public function store(TokenInterface $TokenRepositoryEntity)
    {
        return $this->_TokenMapper->save($TokenRepositoryEntity);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete TokenRepository
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id)
    {
        return $this->_TokenMapper->delete($id);
    }

}
