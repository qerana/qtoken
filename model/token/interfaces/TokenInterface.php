<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qtoken\model\token\interfaces;

/*
  |*****************************************************************************
  | INTERFACE  for [{folder_name}]
  |*****************************************************************************
  |
  | Entity [{folder_name}]
  | @author qDevTools,
  | @date 2020-02-20 06:25:19,
  |*****************************************************************************
 */

interface TokenInterface
{

    public function generateToken(): void;
    
//SETTERS

    
    public function set_id_token(int $id_token): void;

    public function set_id_user(string $id_user): void;

    public function set_token(string $token): void;

    public function set_token_type(string $token_type): void;

    public function set_lifetime(int $lifetime): void;

    public function set_user_agent(string $user_agent): void;

    public function set_remote_address_token(string $remote_address_token): void;

    public function set_sys_timestamp_token(string $sys_timestamp_token): void;

    public function set_sys_token_creationdate(string $sys_token_creationdate): void;

    public function set_token_used(string $token_used): void;

//GETTERS

    public function getUser();
    
    public function get_id_token();

    public function get_id_user();

    public function get_token();

    public function get_token_type();

    public function get_lifetime();

    public function get_user_agent();

    public function get_remote_address_token();

    public function get_sys_timestamp_token();

    public function get_sys_token_creationdate();

    public function get_token_used();
}
