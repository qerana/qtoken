<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qtoken\model\token\interfaces;
use  Qerapp\qtoken\model\token\interfaces\TokenInterface;
/*
  |*****************************************************************************
  | [{model_name}]RepositoryInterface
  |*****************************************************************************
  |
  | Repository INTERFACE [{model_name}]
  | @author qDevTools,
  | @date 2020-02-20 06:25:19,
  |*****************************************************************************
 */

interface TokenRepositoryInterface 
{
    
    public function findAll(array $conditions = [],array $options = []);
    
     public function findById_token(int  $id_token,array $options = []);
 public function findById_user(string $id_user,array $options = []);
 public function findByToken(string $token,array $options = []);
 public function findByToken_type(string $token_type,array $options = []);
 public function findByLifetime(int  $lifetime,array $options = []);
 public function findByUser_agent(string $user_agent,array $options = []);
 public function findByRemote_address_token(string $remote_address_token,array $options = []);
 public function findBySys_timestamp_token(string $sys_timestamp_token,array $options = []);
 public function findBySys_token_creationdate(string $sys_token_creationdate,array $options = []);
 public function findByToken_used(string $token_used,array $options = []);

    
    public function store(TokenInterface $User);
    
    public function remove($id);
 
 
}