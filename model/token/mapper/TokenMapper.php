<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qtoken\model\token\mapper;

use Ada\adapters\AdapterInterface,
    Ada\adapters\PDOAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qtoken\model\token\entity\TokenEntity,
    Qerapp\qtoken\model\token\interfaces\TokenMapperInterface,
    Qerapp\qtoken\model\token\interfaces\TokenInterface;

//RELATED-NAMESPACES    

/*
  |*****************************************************************************
  | MAPPER CLASS for TokenMapperEntity
  |*****************************************************************************
  |
  | MAPPER TokenMapper
  | @author qDevTools,
  | @date 2020-02-20 06:25:19,
  |*****************************************************************************
 */

class TokenMapper extends AdaDataMapper implements TokenMapperInterface
{

    //RELATED-MAPPERS

    public function __construct(AdapterInterface $Adapter = null)
    {

        $this->_Adapter = (is_null($Adapter) ) ? new PDOAdapter(\Ada\SqlPDO::singleton(), 'qtokens') : $Adapter;

        //RELATED-MAPPER-OBJECT


        parent::__construct($this->_Adapter);
    }

    /*
     * -------------------------------------------------------------------------
     * Find by id
     * -------------------------------------------------------------------------
     * @param int $id
     * @return EntityObject
     */

    public function findById(int $id)
    {
        $row = $this->_Adapter->find(['id_token' => $id], ['fetch' => 'one']);

        // if row exists , the create a new entity, otherwise null is returned
        if (!$row) {
            return null;
        } else {
            return $this->createEntity($row);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(TokenInterface $Token)
    {

        $data = parent::getDataObject($Token);

        if (is_null($Token->id_token)) {
            $Token->id_token = $this->_Adapter->insert($data);
        } else {
            $this->_Adapter->update($data, ['id_token' => $Token->id_token]);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete($Token)
    {

        // if $id is a object and a UserEntity  
        if ($Token instanceof TokenInterface) {
            $Token = $Token->id_token;
        }

        return $this->_Adapter->delete(['id_token' => $Token]);
    }

    /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): TokenEntity
    {
        $TokenEntity = new TokenEntity($row);
        return $TokenEntity;
    }

}
