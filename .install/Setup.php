<?php

/*
 * This file is part of Qtoken
 * Copyright (C) 2019-2020  tau  tay@qerana.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Setup {

    public function run() {
     
        // copy xml structure
        $this->copyXmlStructure();
    }

    /**
     *  Create xml structure to _DATA_/xml/qaccess
     */
    private function copyXmlStructure() {


        $dir_target = realpath(__DATA__) . '/xml/qtoken/';
        $dir_source = realpath(__QERAPPSFOLDER__ . 'qtoken/.install/_data/xml/');
        shell_exec('cp -r ' . $dir_source . ' ' . $dir_target);
    }

}
