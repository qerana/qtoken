

CREATE TABLE IF NOT EXISTS `qtokens` (
  `id_token` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(45) NOT NULL DEFAULT '0',
  `token` varchar(30) NOT NULL,
  `token_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1- token de activation\n2- token de recovery\n3- other token',
  `lifetime` int(10) DEFAULT NULL,
  `user_agent` text COMMENT 'el navegador que origina el token.',
  `remote_address_token` varchar(100) DEFAULT NULL,
  `sys_timestamp_token` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sys_token_creationdate` datetime DEFAULT NULL,
  `token_used` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1- usado\n2- pendiente',
  `token_info` text DEFAULT NULL COMMENT 'store token info, eg. ids, user, etc',
  PRIMARY KEY (`id_token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
